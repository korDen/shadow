package com.example.dkoroskin.flatview;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TestActivity extends Activity {

    private FrameLayout mParent1;
    private FrameLayout mParent2;
    private int mNumTestsRun;
    private final TestCanvas mTextViewCanvas = new TestCanvas();
    private final TestCanvas mShadowViewCanvas = new TestCanvas();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        mTextView = new TextView(this);

        // some layout params are necessary
        // TODO: test a few different combinations here to make sure they have no effect on measure
        mTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mShadowTextView = new ShadowTextView();

        mTextViewBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        mShadowTextViewBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);

        long begin = System.currentTimeMillis();

        if (!test("hello world", View.MeasureSpec.AT_MOST, 100, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        if (!test("hello\nworld", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        // BreakStrategy
        if (!test("hello", View.MeasureSpec.AT_MOST, 20, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        // Inexact width
        if (!test("hello\nworld", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        if (!test("hello\nworld", View.MeasureSpec.AT_MOST, 60, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        // AT_MOST
        if (!test("hello\nworld", View.MeasureSpec.AT_MOST, 200, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        // maxLines 0
        if (!test(null, View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, 0, true)) {
            return;
        }
        if (!test(null, View.MeasureSpec.EXACTLY, 10, View.MeasureSpec.EXACTLY, 10, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 16, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 16, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 16, 1, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 64, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 66, 0, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 67, 0, true)) {
            return;
        }
        if (!test(".", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true)) {
            return;
        }
        // clipping
        if (!test("hello", View.MeasureSpec.AT_MOST, 10, View.MeasureSpec.EXACTLY, 100, 1, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 100, View.MeasureSpec.UNSPECIFIED, 0, 1, true)) {
            return;
        }
        if (!test(null, View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        if (!test(null, View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 10, Integer.MAX_VALUE, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true)) {
            return;
        }
        // quirk!
        if (!test("hello", View.MeasureSpec.AT_MOST, 10, View.MeasureSpec.EXACTLY, 294, 1, true)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 10, View.MeasureSpec.EXACTLY, 294+100, 1, true, 0, 100, 0, 0)) {
            return;
        }
        // includeFontPadding = false
        if (!test("hello", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, false)) {
            return;
        }
        // padding
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true, 1, 2, 4, 8)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true, 11, 22, 44, 88)) {
            return;
        }
        if (!test(null, View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true, 11, 22, 44, 88)) {
            return;
        }
        // match clipBottom
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 68, Integer.MAX_VALUE, true, 0, 0, 0, 2)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 68, Integer.MAX_VALUE, true, 0, 0, 0, 3)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 67, Integer.MAX_VALUE, true, 0, 0, 0, 2)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 67, Integer.MAX_VALUE, true, 0, 0, 0, 0, null, Gravity.RIGHT | Gravity.TOP)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 79, 2, true, 17, 19, 23, 29, null, Gravity.LEFT | Gravity.CENTER_VERTICAL)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 79, 0, true, 17, 19, 23, 29, null, Gravity.LEFT | Gravity.CENTER_VERTICAL)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 49, 0, true, 17, 19, 23, 29, null, Gravity.RIGHT | Gravity.TOP)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.AT_MOST, 278, View.MeasureSpec.EXACTLY, 142, 1, true, 17, 19, 23, 29, null, Gravity.RIGHT | Gravity.TOP)) {
            return;
        }
        // end match clipBottom
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.AT_MOST, 64, Integer.MAX_VALUE, true, 0, 0, 0, 22)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 100, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true, 11, 22, 44, 88)) {
            return;
        }
        if (!test("hello\nworld", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.EXACTLY, 0, Integer.MAX_VALUE, true, 11, 22, 44, 88)) {
            return;
        }
        if (!test(".", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 2, true, 0, 0, 0, 0)) {
            return;
        }
        if (!test(".", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true, 0, 0, 0, 0)) {
            return;
        }
        if (!test(".", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true, 11, 0, 0, 0)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 81, View.MeasureSpec.EXACTLY, 264, Integer.MAX_VALUE, true, 11, 22, 44, 88)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 10, View.MeasureSpec.UNSPECIFIED, 0, Integer.MAX_VALUE, true, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        // ellipsize
        if (!test("hello", View.MeasureSpec.AT_MOST, 10, View.MeasureSpec.EXACTLY, 64, 0, true, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 10, 0, false, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        if (!test("hello\nworld", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.UNSPECIFIED, 0, 1, true, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        if (!test("hello\nworld", View.MeasureSpec.AT_MOST, 200, View.MeasureSpec.UNSPECIFIED, 0, 1, true, 0, 0, 0, 0, TextUtils.TruncateAt.END)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 89, View.MeasureSpec.EXACTLY, 1, 1, true, 0, 0, 0, 0, TextUtils.TruncateAt.MIDDLE)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 88, View.MeasureSpec.EXACTLY, 1, 1, true, 0, 0, 0, 0, TextUtils.TruncateAt.MIDDLE)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 40, View.MeasureSpec.EXACTLY, 64, 2, true, 0, 0, 0, 0, TextUtils.TruncateAt.START)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 39, View.MeasureSpec.EXACTLY, 64, 2, true, 0, 0, 0, 0, TextUtils.TruncateAt.START)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.AT_MOST, 100, View.MeasureSpec.EXACTLY, 1, Integer.MAX_VALUE, true, 0, 0, 0, 0, null)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.AT_MOST, 99, View.MeasureSpec.EXACTLY, 1, Integer.MAX_VALUE, true, 0, 0, 0, 0, null)) {
            return;
        }
        // hyphenation
        if (!test("qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,", View.MeasureSpec.AT_MOST, 1026, View.MeasureSpec.EXACTLY, 64, Integer.MAX_VALUE, true, 0, 0, 0, 0, null)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.EXACTLY, 270, View.MeasureSpec.EXACTLY, 100, Integer.MAX_VALUE, true, 0, 0, 0, 0, null, Gravity.BOTTOM | Gravity.RIGHT)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 1, Integer.MAX_VALUE, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
           return;
        }
        if (!test("hello", View.MeasureSpec.EXACTLY, 126, View.MeasureSpec.EXACTLY, 1, Integer.MAX_VALUE, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 27, View.MeasureSpec.EXACTLY, 194, Integer.MAX_VALUE, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 49, 0, true, 17, 19, 23, 29, null, Gravity.RIGHT | Gravity.CENTER_VERTICAL)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 69, View.MeasureSpec.EXACTLY, 112, 1, true, 17, 19, 23, 29, TextUtils.TruncateAt.END, Gravity.RIGHT | Gravity.CENTER_VERTICAL)) {
            return;
        }
        if (!test("hello", View.MeasureSpec.AT_MOST, 92, View.MeasureSpec.EXACTLY, 199, 2, true, 17, 19, 23, 29, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        // clipTop
        if (!test("الشهادتان في الإسلام هما الشهادة بأن \"لا إله إلا الله\" والشهادة بأن \"محمدا رسول الله\"  ( سماع). بحسب الشريعة الإسلامية فإن الشخص يدخل الإسلام بمجرد شهادته بهذا القول أي قوله لا إله إلا الله\" و\"محمد رسول الله\" موقنا ", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 49, Integer.MAX_VALUE, false, 17, 19, 23, 29, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        // ellipsizedWidth
        if (!test("الشهادتان في الإسلام هما الشهادة بأن \"لا إله إلا الله\" والشهادة بأن \"محمدا رسول الله\"  ( سماع). بحسب الشريعة الإسلامية فإن الشخص يدخل الإسلام بمجرد شهادته بهذا القول أي قوله لا إله إلا الله\" و\"محمد رسول الله\" موقنا ", View.MeasureSpec.AT_MOST, 39, View.MeasureSpec.EXACTLY, 11724, Integer.MAX_VALUE, true, 17, 19, 23, 29, TextUtils.TruncateAt.END, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        // gravity quirks
        if (!test("hello world", View.MeasureSpec.AT_MOST, 238, View.MeasureSpec.EXACTLY, 123, 1, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.TOP)) {
            return;
        }
        if (!test("hello world", View.MeasureSpec.AT_MOST, 238, View.MeasureSpec.EXACTLY, 123, 1, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)) {
            return;
        }
        // especially this one, ugh!
        if (!test("hello world", View.MeasureSpec.AT_MOST, 238, View.MeasureSpec.EXACTLY, 123, 1, true, 0, 0, 0, 0, null, Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL)) {
            return;
        }
        // clipRect doesn't intersect
        if (!test("hello world", View.MeasureSpec.UNSPECIFIED, 0, View.MeasureSpec.EXACTLY, 1, 0, true, 17, 19, 23, 29, null, Gravity.RIGHT | Gravity.TOP)) {
            return;
        }
        if (!testMeasureTextView()) {
            return;
        }

        long end = System.currentTimeMillis();

        mParent1 = new FrameLayout(this);
        mParent2 = new FrameLayout(this);

        ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.simple_text_view, mParent1, true);
        TextView textView = (TextView) viewGroup.getChildAt(0);
        textView.setText(textView.getText().toString() + "\nTests ran: " + mNumTestsRun + ".\nTime taken: " + (end - begin) / 1000 + " s.\nBounds cache size: " + TestCanvas.CACHE.size());

        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        mParent1.measure(measureSpec, measureSpec);
        mParent1.layout(0, 0, mParent1.getMeasuredWidth(), mParent1.getMeasuredHeight());

        Bitmap origBitmap = Bitmap.createBitmap(mParent1.getWidth(), mParent1.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas origCanvas = new Canvas(origBitmap);
        mParent1.draw(origCanvas);

        ImageView origView = (ImageView) findViewById(R.id.orig_view);
        origView.setImageBitmap(origBitmap);
    }

    private static String modeToString(int mode) {
        if (mode == View.MeasureSpec.UNSPECIFIED) {
            return "UNSPECIFIED";
        } else if (mode == View.MeasureSpec.AT_MOST) {
            return "AT_MOST";
        } else if (mode == View.MeasureSpec.EXACTLY) {
            return "EXACTLY";
        } else {
            throw new RuntimeException("Unknown mode " + mode);        }
    }

    Bitmap mTextViewBitmap;
    Bitmap mShadowTextViewBitmap;
    TextView mTextView;
    ShadowTextView mShadowTextView;

    private boolean test(CharSequence text, int widthMode, int widthSize, int heightMode, int heightSize, int maxLines, boolean includeFontPadding) {
        return test(text, widthMode, widthSize, heightMode, heightSize, maxLines, includeFontPadding, 0, 0, 0, 0, null);
    }

    String errorString(int widthMode, int widthSize, int heightMode, int heightSize) {
        CharSequence text = mTextView.getText();
        int maxLines = mTextView.getMaxLines();
        boolean includeFontPadding = mTextView.getIncludeFontPadding();
        int paddingLeft = mTextView.getPaddingLeft();
        int paddingTop = mTextView.getPaddingTop();
        int paddingRight = mTextView.getPaddingRight();
        int paddingBottom = mTextView.getPaddingBottom();
        TextUtils.TruncateAt ellipsize = mTextView.getEllipsize();

        return "test(" + ((text == null) ? "null" : "\"" + text + "\"") + ", View.MeasureSpec." + modeToString(widthMode) + ", " + widthSize + ", View.MeasureSpec." + modeToString(heightMode) + ", " + heightSize + ", " + (maxLines == Integer.MAX_VALUE ? "Integer.MAX_VALUE" : maxLines) + ", " + includeFontPadding + ", " + paddingLeft + ", " + paddingTop + ", " + paddingRight + ", " + paddingBottom + ", " + (ellipsize == null ? null : "TextUtils.TruncateAt." + ellipsize) + ", " + gravityToString(mTextView.getGravity()) + ")";
    }

    private boolean test(CharSequence text, int widthMode, int widthSize, int heightMode, int heightSize, int maxLines, boolean includeFontPadding, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        return test(text, widthMode, widthSize, heightMode, heightSize, maxLines, includeFontPadding, paddingLeft, paddingTop, paddingRight, paddingBottom, null);
    }

    private boolean test(CharSequence text, int widthMode, int widthSize, int heightMode, int heightSize, int maxLines, boolean includeFontPadding, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom, TextUtils.TruncateAt ellipsize) {
        return test(text, widthMode, widthSize, heightMode, heightSize, maxLines, includeFontPadding, paddingLeft, paddingTop, paddingRight, paddingBottom, ellipsize, Gravity.LEFT | Gravity.TOP);
    }

    private boolean test(CharSequence text, int widthMode, int widthSize, int heightMode, int heightSize, int maxLines, boolean includeFontPadding, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom, TextUtils.TruncateAt ellipsize, int gravity) {
        mTextView.setGravity(gravity);
        mShadowTextView.setGravity(gravity);

        mTextView.setText(text);
        mShadowTextView.setText(text);

        mTextView.setMaxLines(maxLines);
        mShadowTextView.setMaxLines(maxLines);

        mTextView.setEllipsize(ellipsize);
        mShadowTextView.setEllipsize(ellipsize);

        mTextView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        mShadowTextView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);

        if (ShadowTextView.STRICT_MEASURE) {
            // reset layout because it affects measure
            mTextView.setIncludeFontPadding(!includeFontPadding);
        }

        mTextView.setIncludeFontPadding(includeFontPadding);
        mShadowTextView.setIncludeFontPadding(includeFontPadding);

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(widthSize, widthMode);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(heightSize, heightMode);

        mTextView.measure(widthMeasureSpec, heightMeasureSpec);
        mShadowTextView.measure(widthMeasureSpec, heightMeasureSpec);

        return testOnly(widthMode, widthSize, heightMode, heightSize);
    }

    String gravityToString(int gravity) {
        String result;
        switch (gravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
            case Gravity.LEFT:
                result = "Gravity.LEFT";
                break;
            case Gravity.RIGHT:
                result = "Gravity.RIGHT";
                break;
            case Gravity.CENTER_HORIZONTAL:
                result = "Gravity.CENTER_HORIZONTAL";
                break;
            default:
                throw new RuntimeException();
        }

        switch (gravity & Gravity.VERTICAL_GRAVITY_MASK) {
            case Gravity.TOP:
                result += " | Gravity.TOP";
                break;
            case Gravity.BOTTOM:
                result += " | Gravity.BOTTOM";
                break;
            case Gravity.CENTER_VERTICAL:
                result += " | Gravity.CENTER_VERTICAL";
                break;
            default:
                throw new RuntimeException();
        }

        return result;
    }

    private boolean testOnly(int widthMode, int widthSize, int heightMode, int heightSize) {
        ++mNumTestsRun;

        int textViewMeasuredWidth = mTextView.getMeasuredWidth();
        int textViewMeasuredHeight = mTextView.getMeasuredHeight();
        int shadowViewMeasuredWidth = mShadowTextView.getMeasuredWidth();
        int shadowViewMeasuredHeight = mShadowTextView.getMeasuredHeight();

        mTextView.layout(0, 0, textViewMeasuredWidth, textViewMeasuredHeight);
        mShadowTextView.layout(0, 0, shadowViewMeasuredWidth, shadowViewMeasuredHeight);

        if (widthMode == View.MeasureSpec.EXACTLY || ShadowTextView.STRICT_MEASURE) {
            if (textViewMeasuredWidth != shadowViewMeasuredWidth) {
                error("Measure mismatch for " + errorString(widthMode, widthSize, heightMode, heightSize) + " wrong width " + textViewMeasuredWidth + " != " + shadowViewMeasuredWidth);
                drawTextViewAndShadowTextView(true);
                return false;
            }
        } else {
            if (textViewMeasuredWidth < shadowViewMeasuredWidth) {
                error("Measure mismatch for " + errorString(widthMode, widthSize, heightMode, heightSize) + " wrong width " + mTextView.getMeasuredWidth() + " < " + mShadowTextView.getMeasuredWidth());
                drawTextViewAndShadowTextView(true);
                return false;
            }
        }

        if (textViewMeasuredHeight != shadowViewMeasuredHeight) {
            error("Measure mismatch for " + errorString(widthMode, widthSize, heightMode, heightSize) + " wrong height " + mTextView.getMeasuredHeight() + " != " + mShadowTextView.getMeasuredHeight());
            drawTextViewAndShadowTextView(true);
            return false;
        }

        if (textViewMeasuredWidth == 0 || textViewMeasuredHeight == 0) {
            return true;
        }

        if (!compareViewsVisuallyFast()) {
            //if (!compareViewsVisuallySlow()) {
            compareViewsVisuallyFast();
            error("Wrong rendering for " + errorString(widthMode, widthSize, heightMode, heightSize));
            drawTextViewAndShadowTextView(true);
            return false;
            //}
        }

        return true;
    }

    private boolean compareViewsVisuallyFast() {
        mTextViewCanvas.reset(mTextView.getMeasuredWidth(), mTextView.getMeasuredHeight());
        mTextView.draw(mTextViewCanvas);

        mShadowViewCanvas.reset(mShadowTextView.getMeasuredWidth(), mShadowTextView.getMeasuredHeight());
        mShadowTextView.draw(mShadowViewCanvas);

        return mTextViewCanvas.equals(mShadowViewCanvas);
    }

    private boolean compareViewsVisuallySlow() {
        int width = mTextView.getMeasuredWidth() + 20;
        int height = mTextView.getMeasuredHeight() + 20;

        Bitmap textViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Bitmap shadowTextViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas textViewCanvas = new Canvas(textViewBitmap);
        Canvas shadowTextViewCanvas = new Canvas(shadowTextViewBitmap);

        textViewCanvas.translate(10, 10);
        shadowTextViewCanvas.translate(10, 10);

        mTextView.draw(textViewCanvas);
        mShadowTextView.draw(shadowTextViewCanvas);

        return textViewBitmap.sameAs(shadowTextViewBitmap);
    }

    private boolean drawTextViewAndShadowTextView(final boolean debug) {
        int multiplier = debug ? 5 : 1;

        int width = (mTextView.getMeasuredWidth() + 20) * multiplier;
        int height = (mTextView.getMeasuredHeight() + 20) * multiplier;

        mTextViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mShadowTextViewBitmap = Bitmap.createBitmap(
                debug ? (mShadowTextView.getMeasuredWidth() + 20) * multiplier : width,
                debug ? (mShadowTextView.getMeasuredHeight() + 20) * multiplier : height,
                Bitmap.Config.ARGB_8888);

        Canvas textViewCanvas = new Canvas(mTextViewBitmap) {
            @Override
            public void translate(float dx, float dy) {
                if (debug) {
                    Log.v("Test", "textViewCanvas translate " + dx + " " + dy);
                }
                super.translate(dx, dy);
            }

            @Override
            public boolean clipRect(int left, int top, int right, int bottom) {
                if (debug) {
                    Log.v("Test", "textViewCanvas clipRect " + left + " " + top + " " + right + " " + bottom);
                }
                return super.clipRect(left, top, right, bottom);
            }
            @Override
            public boolean clipRect(float left, float top, float right, float bottom) {
                if (debug) {
                    Log.v("Test", "textViewCanvas clipRect " + left + " " + top + " " + right + " " + bottom);
                }
                return super.clipRect(left, top, right, bottom);
            }
        };
        textViewCanvas.scale(multiplier, multiplier);
        textViewCanvas.translate(10, 10);

        Canvas shadowTextViewCanvas = new Canvas(mShadowTextViewBitmap) {
            @Override
            public void translate(float dx, float dy) {
                if (debug) {
                    Log.v("Test", "shadowTextViewCanvas translate " + dx + " " + dy);
                }
                super.translate(dx, dy);
            }

            @Override
            public boolean clipRect(int left, int top, int right, int bottom) {
                if (debug) {
                    Log.v("Test", "shadowTextViewCanvas clipRect " + left + " " + top + " " + right + " " + bottom);
                }
                return super.clipRect(left, top, right, bottom);
            }

            @Override
            public boolean clipRect(float left, float top, float right, float bottom) {
                if (debug) {
                    Log.v("Test", "shadowTextViewCanvas clipRect " + left + " " + top + " " + right + " " + bottom);
                }
                return super.clipRect(left, top, right, bottom);
            }
        };
        shadowTextViewCanvas.scale(multiplier, multiplier);
        shadowTextViewCanvas.translate(10, 10);

        if (debug) {
            Paint p = new Paint();
            p.setColor(0xFFBBBBBB);
            textViewCanvas.drawRect(0, 0, mTextView.getMeasuredWidth(), mTextView.getMeasuredHeight(), p);
            shadowTextViewCanvas.drawRect(0, 0, mShadowTextView.getMeasuredWidth(), mShadowTextView.getMeasuredHeight(), p);
        }

        mTextView.draw(textViewCanvas);
        mShadowTextView.draw(shadowTextViewCanvas);

        if (debug) {
            ImageView origView = (ImageView) findViewById(R.id.orig_view);
            origView.setImageBitmap(mTextViewBitmap);

            ImageView flatView = (ImageView) findViewById(R.id.flat_view);
            flatView.setImageBitmap(mShadowTextViewBitmap);
        }

        return mTextViewBitmap.sameAs(mShadowTextViewBitmap);
    }

    boolean measureAndTest(int widthMode, int widthSize, int heightMode, int heightSize, boolean includeFontPadding) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(widthSize, widthMode);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(heightSize, heightMode);

        measureTextViewAndShadowTextView(widthMeasureSpec, heightMeasureSpec, includeFontPadding);

        boolean result = testOnly(widthMode, widthSize, heightMode, heightSize);
        if (!result) {
            measureTextViewAndShadowTextView(widthMeasureSpec, heightMeasureSpec, includeFontPadding);
            result = testOnly(widthMode, widthSize, heightMode, heightSize);
        }

        return result;
    }

    void measureTextView(int widthMeasureSpec, int heightMeasureSpec, boolean includeFontPadding) {
        if (ShadowTextView.STRICT_MEASURE) {
            // reset layout
            mTextView.setIncludeFontPadding(!includeFontPadding);
            mTextView.setIncludeFontPadding(includeFontPadding);
        }
        mTextView.measure(widthMeasureSpec, heightMeasureSpec);
    }

    void measureTextViewAndShadowTextView(int widthMeasureSpec, int heightMeasureSpec, boolean includeFontPadding) {
        measureTextView(widthMeasureSpec, heightMeasureSpec, includeFontPadding);
        mShadowTextView.measure(widthMeasureSpec, heightMeasureSpec);
    }

    int measureAndTestHeight(int widthMode, int widthSize, boolean includeFontPadding) {
        measureTextView(View.MeasureSpec.makeMeasureSpec(widthSize, widthMode), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), includeFontPadding);
        int maxWidth = Math.round(mTextView.getMeasuredWidth() * 2.0f);
        int maxHeight = Math.round(mTextView.getMeasuredHeight() * 2.0f);

        for (int height = 0; height < maxHeight; ++height) {
            if (!measureAndTest(widthMode, widthSize, View.MeasureSpec.EXACTLY, height, includeFontPadding)) {
                return -1;
            }
            if (!measureAndTest(widthMode, widthSize, View.MeasureSpec.AT_MOST, height, includeFontPadding)) {
                return -1;
            }
        }

        return maxWidth;
    }

    boolean testMeasureTextView() {
        CharSequence[] texts = new CharSequence[]{
//                null,
//                "hello",
                "hello world",
//                "hello\nworld",
//                "hello\nworld\n!",
//                "qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,",
//                "لا إله إلا الله محمد رسول الله",
//                "hello بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ привет",
//                "الشهادتان في الإسلام هما الشهادة بأن \"لا إله إلا الله\" والشهادة بأن \"محمدا رسول الله\"  ( سماع). بحسب الشريعة الإسلامية فإن الشخص يدخل الإسلام بمجرد شهادته بهذا القول أي قوله لا إله إلا الله\" و\"محمد رسول الله\" موقنا ",
        };

        int[] allMaxLines = new int[] {
                Integer.MAX_VALUE,
                0,
                1,
                2,
        };

        boolean[] includeFontPaddings = new boolean[] {
                true, false,
        };

        TextUtils.TruncateAt[] ellipsizes = new TextUtils.TruncateAt[] {
                null,
                TextUtils.TruncateAt.END,
                TextUtils.TruncateAt.MIDDLE,
                TextUtils.TruncateAt.START,
        };

        int[] gravities = new int[] {
                Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM,
                Gravity.RIGHT | Gravity.TOP,
                Gravity.LEFT | Gravity.CENTER_VERTICAL,
        };

        int[][] paddings = new int[][] {
                new int[] { 0, 0, 0, 0 },
                new int[] { 17, 19, 23, 29 },
        };

        for (CharSequence text : texts) {
            android.util.Log.v("Test", "text: " + text);
            mTextView.setText(text);
            mShadowTextView.setText(text);
            for (int maxLines : allMaxLines) {
                android.util.Log.v("Test", " maxLines: " + maxLines);
                mTextView.setMaxLines(maxLines);
                mShadowTextView.setMaxLines(maxLines);
                for (TextUtils.TruncateAt ellipsize : ellipsizes) {
                    if (maxLines != 1) {
                        if (ellipsize == TextUtils.TruncateAt.MIDDLE || ellipsize == TextUtils.TruncateAt.START) {
                            continue;
                        }
                    }
                    android.util.Log.v("Test", "  ellipsize: " + ellipsize);
                    mTextView.setEllipsize(ellipsize);
                    mShadowTextView.setEllipsize(ellipsize);
                    for (boolean includeFontPadding : includeFontPaddings) {
                        android.util.Log.v("Test", "   includeFontPadding: " + includeFontPadding);
                        mTextView.setIncludeFontPadding(includeFontPadding);
                        mShadowTextView.setIncludeFontPadding(includeFontPadding);
                        for (int[] padding : paddings) {
//                            android.util.Log.v("Test", "    padding: " + ((padding[0] == 0) ? "off" : "on"));
                            mTextView.setPadding(padding[0], padding[1], padding[2], padding[3]);
                            mShadowTextView.setPadding(padding[0], padding[1], padding[2], padding[3]);
                            for (int gravity : gravities) {
//                                android.util.Log.v("Test", "     gravity: " + Integer.toHexString(gravity));
                                mTextView.setGravity(gravity);
                                mShadowTextView.setGravity(gravity);
                                int maxWidth = measureAndTestHeight(View.MeasureSpec.UNSPECIFIED, 0, includeFontPadding);
                                if (maxWidth < 0) {
                                    return false;
                                }

                                for (int width = maxWidth; width >= 0; --width) {
                                    if (measureAndTestHeight(View.MeasureSpec.AT_MOST, width, includeFontPadding) < 0) {
                                        return false;
                                    }

                                    if (measureAndTestHeight(View.MeasureSpec.EXACTLY, width, includeFontPadding) < 0) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    void error(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        android.util.Log.v("Test", message);
        //throw new RuntimeException(message);
    }

    void assertEquals(Object lhs, Object rhs) {
        if (!lhs.equals(rhs)) {
            throw new RuntimeException("" + lhs + " != " + rhs);
        }
    }
}
