package com.example.dkoroskin.flatview;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.GetChars;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by akoroskina on 11.05.16.
 */
public class TestCanvas extends StubCanvas {

    static final HashMap<BoundsCache,Rect> CACHE = new HashMap<>();
    static final BoundsCache ENTRY = new BoundsCache();
    static long sBoundsTime = 0;

    static class BoundsCache {
        char[] text;
        int length;
        int hashCode;

        void reset(char[] text, int length) {
            this.text = text;
            this.length = length;

            int hashCode = length;
            for (int i = 0; i < length; ++i) {
                hashCode = 31 * hashCode + text[i];
            }

            this.hashCode = hashCode;
        }

        @Override
        public boolean equals(Object o) {
            BoundsCache that = (BoundsCache) o;
            if (hashCode != that.hashCode) return false;
            if (length != that.length) return false;
            for (int i = 0; i < length; ++i) {
                if (text[i] != that.text[i]) return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    static class DrawTextRun {
        char[] text = new char[10];
        int length;
        RectF bounds = new RectF();

        void allocateText(int textLength) {
            if (text.length < textLength) {
                text = new char[textLength];
            }
            length = textLength;
        }

        boolean reset(char[] text, int start, int end, float x, float y, Paint paint, RectF clipRect) {
            int length = end - start;
            allocateText(length);
            System.arraycopy(text, start, this.text, 0, length);
            return updateBounds(x, y, paint, clipRect);
        }

        boolean reset(CharSequence text, int start, int end, float x, float y, Paint paint, RectF clipRect) {
            int length = end - start;
            allocateText(length);
            if (text instanceof GetChars) {
                ((GetChars) text).getChars(start, end, this.text, 0);
            } else {
                ((String) text).getChars(start, end, this.text, 0);
            }

            return updateBounds(x, y, paint, clipRect);
        }

        boolean reset(String text, float x, float y, Paint paint, RectF clipRect) {
            int length = text.length();
            allocateText(length);
            text.getChars(0, length, this.text, 0);

            return updateBounds(x, y, paint, clipRect);
        }

        boolean updateBounds(float x, float y, Paint paint, RectF clipRect) {
            bounds.set(getTextBounds(paint));
            bounds.offset(x, y);
            return bounds.intersect(clipRect);
        }

        Rect getTextBounds(Paint paint) {
            ENTRY.reset(text, length);

            Rect rect = CACHE.get(ENTRY);
            if (rect != null) {
                return rect;
            }

            rect = new Rect();
            long begin = System.nanoTime();
            paint.getTextBounds(text, 0, length, rect);
            sBoundsTime += System.nanoTime() - begin;

            char[] textCopy = new char[length];
            for (int i = 0; i < length; ++i) {
                textCopy[i] = text[i];
            }

            BoundsCache entry = new BoundsCache();
            entry.reset(textCopy, length);

            CACHE.put(entry, rect);

            return rect;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DrawTextRun that = (DrawTextRun) o;

            if (length != that.length) return false;
            for (int i = 0; i < length; ++i) {
                if (text[i] != that.text[i]) return false;
            }
            if (!bounds.equals(that.bounds)) return false;
            return true;
        }
    }

//    final TestCanvas mReference;
    final RectF mClipRect = new RectF();
    final ArrayList<DrawTextRun> mTextRuns = new ArrayList<>();
    float mTranslateX;
    float mTranslateY;
    boolean mSaved;
    int mNumTextRuns;
/*
    TestCanvas(TestCanvas reference) {
        mReference = reference;
    }
*/
    void reset(int width, int height) {
        mClipRect.set(0, 0, (float) width, (float) height);
        mTranslateX = 0.0f;
        mTranslateY = 0.0f;
        mNumTextRuns = 0;
        mSaved = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestCanvas that = (TestCanvas) o;
        if (mNumTextRuns == 0) {
            return that.mNumTextRuns == 0;
        }

        if (mTranslateX != that.mTranslateX) return false;
        if (mTranslateY != that.mTranslateY) return false;
        if (!mClipRect.equals(that.mClipRect)) return false;
        if (mNumTextRuns != that.mNumTextRuns) return false;
        for (int i = 0; i < mNumTextRuns; ++i) {
            if (!mTextRuns.get(i).equals(that.mTextRuns.get(i))) return false;
        }

        return true;
    }

    @Override
    public boolean isHardwareAccelerated() {
        return true;
    }

    @Override
    public int save() {
        if (mSaved) {
            throw new RuntimeException();
        }
        mSaved = true;
        return 0;
    }

    @Override
    public void restore() {
    }

    @Override
    public boolean clipRect(float left, float top, float right, float bottom) {
        if (!mClipRect.intersect(mTranslateX + left, mTranslateY + top, mTranslateX + right, mTranslateY + bottom)) {
            mClipRect.setEmpty();
        }
        return !mClipRect.isEmpty();
    }

    @Override
    public boolean clipRect(int left, int top, int right, int bottom) {
        return clipRect((float) left, (float) top, (float) right, (float) bottom);
    }

    @Override
    public void translate(float dx, float dy) {
        mTranslateX += dx;
        mTranslateY += dy;
    }

    @Override
    public boolean getClipBounds(Rect bounds) {
        bounds.set((int) (mClipRect.left - mTranslateX), (int) (mClipRect.top - mTranslateY),
                (int) (mClipRect.right - mTranslateX), (int) (mClipRect.bottom - mTranslateY));
        return !bounds.isEmpty();
    }

    @Override
    public void drawText(CharSequence text, int start, int end, float x, float y, Paint paint) {
        if (start == end) {
            return;
        }
        if (allocateTextRun().reset(text, start, end, x + mTranslateX, y + mTranslateY, paint, mClipRect)) {
            ++mNumTextRuns;
        }
    }

    @Override
    public void drawText(String text, float x, float y, Paint paint) {
        if (text.isEmpty()) {
            return;
        }
        //DrawTextRun referenceRun = (mReference == null) ? null : mReference.getTextRun(mNumTextRuns);
        if (allocateTextRun().reset(text, x + mTranslateX, y + mTranslateY, paint, mClipRect)) {
            ++mNumTextRuns;
        }
    }

    @Override
    public void drawTextRun(char[] text, int index, int count, int contextIndex, int contextCount, float x, float y, boolean isRtl, Paint paint) {
        if (index != contextIndex || count != contextCount) {
            throw new RuntimeException("not tested");
        }

        if (allocateTextRun().reset(text, index, index + count, x + mTranslateX, y + mTranslateY, paint, mClipRect)) {
            ++mNumTextRuns;
        }
    }

    private DrawTextRun allocateTextRun() {
        final DrawTextRun textRun;
        if (mTextRuns.size() > mNumTextRuns) {
            textRun = mTextRuns.get(mNumTextRuns);
        } else {
            textRun = new DrawTextRun();
            mTextRuns.add(textRun);
        }

        return textRun;
    }
}
