package com.example.dkoroskin.flatview;

import android.view.View;

/**
 * Created by dkoroskin on 4/26/2016.
 */
public abstract class ShadowView {
    private ShadowViewGroup.LayoutParams mLayoutParams;
    private int mMeasuredWidth;
    private int mMeasuredHeight;
    private int mLeft;
    private int mTop;
    private int mRight;
    private int mBottom;
    private int mPaddingLeft;
    private int mPaddingTop;
    private int mPaddingRight;
    private int mPaddingBottom;

    public ShadowViewGroup.LayoutParams getLayoutParams() {
        return mLayoutParams;
    }

    protected final void setMeasuredDimensions(int measuredWidth, int measuredHeight) {
        mMeasuredWidth = measuredWidth;
        mMeasuredHeight = measuredHeight;
    }

    public final void requestLayout() {
        // TODO: implement
    }

    public final void invalidate() {
        // TODO: implement
    }

    private static int getSizeForEmpty(int measureSpec) {
        if (View.MeasureSpec.getMode(measureSpec) == View.MeasureSpec.EXACTLY) {
            return View.MeasureSpec.getSize(measureSpec);
        }
        return 0;
    }

    public final void setDefaultMeasuredDimensions(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimensions(getSizeForEmpty(widthMeasureSpec), getSizeForEmpty(heightMeasureSpec));
    }

    public final int getLeft() {
        return mLeft;
    }

    public final int getTop() {
        return mTop;
    }

    public final int getRight() {
        return mRight;
    }

    public final int getBottom() {
        return mBottom;
    }

    public final int getWidth() {
        return mRight - mLeft;
    }

    public final int getHeight() {
        return mBottom - mTop;
    }

    public final int getPaddingLeft() {
        return mPaddingLeft;
    }

    public final int getPaddingTop() {
        return mPaddingTop;
    }

    public final int getPaddingRight() {
        return mPaddingRight;
    }

    public final int getPaddingBottom() {
        return mPaddingBottom;
    }

    public final int getHorizontalPadding() {
        return mPaddingLeft + mPaddingRight;
    }

    public final int getVerticalPadding() {
        return mPaddingBottom + mPaddingTop;
    }

    public final void setPadding(int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        if (mPaddingLeft == paddingLeft && mPaddingTop == paddingTop && mPaddingRight ==  paddingRight && mPaddingBottom == paddingBottom) {
            return;
        }

        mPaddingLeft = paddingLeft;
        mPaddingTop = paddingTop;
        mPaddingRight = paddingRight;
        mPaddingBottom = paddingBottom;

        requestLayout();
    }

    public final int getMeasuredWidth() {
        return mMeasuredWidth;
    }

    public final int getMeasuredHeight() {
        return mMeasuredHeight;
    }

    public final void layout(int left, int top, int right, int bottom) {
        mLeft = left;
        mTop = top;
        mRight = right;
        mBottom = bottom;

        onLayout(left, top, right, bottom);
    }

    protected abstract void onLayout(int left, int top, int right, int bottom);
}
