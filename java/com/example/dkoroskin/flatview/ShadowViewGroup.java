package com.example.dkoroskin.flatview;

/**
 * Created by dkoroskin on 4/26/2016.
 */
public abstract class ShadowViewGroup {
    public static class LayoutParams {
        public int width;
        public int height;
    }
}
