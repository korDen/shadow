package com.example.dkoroskin.flatview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;

public class StubCanvas extends Canvas {
    public native boolean isHardwareAccelerated();

    public native void setBitmap(Bitmap bitmap);
    public native void setViewport(int width, int height);
    public native void setHighContrastText(boolean highContrastText);
    public native void insertReorderBarrier();
    public native void insertInorderBarrier();
    public native boolean isOpaque();
    public native int getWidth();
    public native int getHeight();
    public native int getDensity();
    public native void setDensity(int density);
    public native void setScreenDensity(int density);
    public native int getMaximumBitmapWidth();
    public native int getMaximumBitmapHeight();
    public native int save();
    public native int save(int saveFlags);
    public native int saveLayer(RectF bounds, Paint paint, int saveFlags);
    public native int saveLayer(RectF bounds, Paint paint);
    public native int saveLayer(float left, float top, float right, float bottom, Paint paint, int saveFlags);
    public native int saveLayer(float left, float top, float right, float bottom, Paint paint);
    public native int saveLayerAlpha(RectF bounds, int alpha, int saveFlags);
    public native int saveLayerAlpha(RectF bounds, int alpha);
    public native int saveLayerAlpha(float left, float top, float right, float bottom, int alpha, int saveFlags);
    public native int saveLayerAlpha(float left, float top, float right, float bottom, int alpha);
    public native void restore();
    public native int getSaveCount();
    public native void restoreToCount(int saveCount);
    public native void translate(float dx, float dy);
    public native void scale(float sx, float sy);
    public native void rotate(float degrees);
    public native void skew(float sx, float sy);
    public native void concat(Matrix matrix);
    public native void setMatrix(Matrix matrix);
    public native void getMatrix(Matrix ctm);
    public native boolean clipRect(RectF rect, Region.Op op);
    public native boolean clipRect(Rect rect, Region.Op op);
    public native boolean clipRect(RectF rect);
    public native boolean clipRect(Rect rect);
    public native boolean clipRect(float left, float top, float right, float bottom, Region.Op op);
    public native boolean clipRect(float left, float top, float right, float bottom);
    public native boolean clipRect(int left, int top, int right, int bottom);
    public native boolean clipPath(Path path, Region.Op op);
    public native boolean clipPath(Path path);
    public native boolean clipRegion(Region region, Region.Op op);
    public native boolean clipRegion(Region region);
    public native DrawFilter getDrawFilter();
    public native void setDrawFilter(DrawFilter filter);
    public native boolean quickReject(RectF rect, EdgeType type);
    public native boolean quickReject(Path path, EdgeType type);
    public native boolean quickReject(float left, float top, float right, float bottom, EdgeType type);
    public native boolean getClipBounds(Rect bounds);
    public native void drawRGB(int r, int g, int b);
    public native void drawARGB(int a, int r, int g, int b);
    public native void drawColor(int color);
    public native void drawColor(int color, PorterDuff.Mode mode);
    public native void drawPaint(Paint paint);
    public native void drawPoints(float[] pts, int offset, int count, Paint paint);
    public native void drawPoints(float[] pts, Paint paint);
    public native void drawPoint(float x, float y, Paint paint);
    public native void drawLine(float startX, float startY, float stopX, float stopY, Paint paint);
    public native void drawLines(float[] pts, int offset, int count, Paint paint);
    public native void drawLines(float[] pts, Paint paint);
    public native void drawRect(RectF rect, Paint paint);
    public native void drawRect(Rect r, Paint paint);
    public native void drawRect(float left, float top, float right, float bottom, Paint paint);
    public native void drawOval(RectF oval, Paint paint);
    public native void drawOval(float left, float top, float right, float bottom, Paint paint);
    public native void drawCircle(float cx, float cy, float radius, Paint paint);
    public native void drawArc(RectF oval, float startAngle, float sweepAngle, boolean useCenter, Paint paint);
    public native void drawArc(float left, float top, float right, float bottom, float startAngle, float sweepAngle, boolean useCenter, Paint paint);
    public native void drawRoundRect(RectF rect, float rx, float ry, Paint paint);
    public native void drawRoundRect(float left, float top, float right, float bottom, float rx, float ry, Paint paint);
    public native void drawPath(Path path, Paint paint);
    public native void drawPatch(NinePatch patch, Rect dst, Paint paint);
    public native void drawPatch(NinePatch patch, RectF dst, Paint paint);
    public native void drawBitmap(Bitmap bitmap, float left, float top, Paint paint);
    public native void drawBitmap(Bitmap bitmap, Rect src, RectF dst, Paint paint);
    public native void drawBitmap(Bitmap bitmap, Rect src, Rect dst, Paint paint);
    public native void drawBitmap(int[] colors, int offset, int stride, float x, float y,
                           int width, int height, boolean hasAlpha, Paint paint);
    public native void drawBitmap(int[] colors, int offset, int stride, int x, int y,
                           int width, int height, boolean hasAlpha, Paint paint);
    public native void drawBitmap(Bitmap bitmap, Matrix matrix, Paint paint);
    public native void drawBitmapMesh(Bitmap bitmap, int meshWidth, int meshHeight,
                               float[] verts, int vertOffset, int[] colors, int colorOffset,
                               Paint paint);
    public native void drawVertices(VertexMode mode, int vertexCount, float[] verts,
                             int vertOffset, float[] texs, int texOffset, int[] colors,
                             int colorOffset, short[] indices, int indexOffset, int indexCount, Paint paint);
    public native void drawText(char[] text, int index, int count, float x, float y, Paint paint);
    public native void drawText(String text, float x, float y, Paint paint);
    public native void drawText(String text, int start, int end, float x, float y, Paint paint);
    public native void drawText(CharSequence text, int start, int end, float x, float y, Paint paint);
    public native void drawTextRun(char[] text, int index, int count, int contextIndex, int contextCount, float x, float y, boolean isRtl, Paint paint);
    public native void drawTextRun(CharSequence text, int start, int end, int contextStart, int contextEnd, float x, float y, boolean isRtl, Paint paint);
    public native void drawPosText(char[] text, int index, int count, float[] pos, Paint paint);
    public native void drawPosText(String text, float[] pos, Paint paint);
    public native void drawTextOnPath(char[] text, int index, int count, Path path, float hOffset, float vOffset, Paint paint);
    public native void drawTextOnPath(String text, Path path, float hOffset,
                               float vOffset, Paint paint);
    public native void drawPicture(Picture picture);
    public native void drawPicture(Picture picture, RectF dst);
    public native void drawPicture(Picture picture, Rect dst);
    public native void release();
}