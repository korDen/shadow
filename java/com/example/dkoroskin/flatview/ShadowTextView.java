package com.example.dkoroskin.flatview;

import android.content.Context;
import android.graphics.Canvas;
import android.text.BoringLayout;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

/**
 * Created by dkoroskin on 4/26/2016.
 */
public final class ShadowTextView extends ShadowView {

    public static final boolean STRICT_MEASURE = true;
    public static final boolean BUG_COMPATIBLE = true;

    private static BoringLayout.Metrics UNKNOWN_BORING = new BoringLayout.Metrics();

    private static final TextPaint DEFAULT_PAINT;
    private static BoringLayout.Metrics BORING_METRICS;

    private static final Layout.Alignment ALIGN_LEFT;
    private static final Layout.Alignment ALIGN_RIGHT;

    static {
        DEFAULT_PAINT = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
        // TODO: these should not be hardcoded
        DEFAULT_PAINT.setTextSize(49.0f);
        DEFAULT_PAINT.setColor(0x8A000000);
        DEFAULT_PAINT.density = 3.5f;

        Layout.Alignment[] alignments = Layout.Alignment.values();
        Layout.Alignment alignLeft = Layout.Alignment.ALIGN_NORMAL;
        Layout.Alignment alignRight = Layout.Alignment.ALIGN_OPPOSITE;

        for (Layout.Alignment alignment : Layout.Alignment.values()) {
            if (alignment.toString().equals("ALIGN_LEFT")) {
                alignLeft = alignment;
            } else if (alignment.toString().equals("ALIGN_RIGHT")) {
                alignRight = alignment;
            }
        }

        ALIGN_LEFT = alignLeft;
        ALIGN_RIGHT = alignRight;
    }

    private CharSequence mText = "";
    private BoringLayout.Metrics mBoringMetrics = UNKNOWN_BORING;
    private TextPaint mPaint = DEFAULT_PAINT;
    private int mMaxLines = Integer.MAX_VALUE;
    private boolean mIncludeFontPadding = true;
    private TextUtils.TruncateAt mEllipsize;

    private Layout mLayout;

    // Result of calculateLayoutWidth().
    // Cached to avoid recomputing it on every draw().
    private int mLayoutWidth;
    private boolean mLayoutIncludeFontPadding;
    private TextUtils.TruncateAt mLayoutEllipsize;
    private Layout.Alignment mLayoutAlignment;
    private int mLayoutMaxLines;
    private int mGravity = Gravity.LEFT | Gravity.TOP;

    public ShadowTextView() {
    }

    public ShadowTextView(Context context, AttributeSet attrs) {
        // TODO: implement proper attrs parsing
    }

    @Override
    protected final void onLayout(int left, int top, int right, int bottom) {
        if (mText.length() == 0) {
            mLayout = null;
            return;
        }

        int width = right - left - getHorizontalPadding();

        // lazily create Layout
        if (mBoringMetrics != null) {
            // possibly BoringLayout
            if (width >= mBoringMetrics.width) {
                ensureBoringLayout(width);
                return;
            }

            // Technically, we can use BoringLayout if mMaxLines == 1
            // However, to achieve match TextView rendering, we need to truncate it
            // Which is currently not implemented
            // if (mMaxLines == 1) { ... }
        }

        ensureStaticLayout(width);
    }

    private int getFudgedPaddingRight() {
        // Add sufficient space for cursor and tone marks
        int cursorWidth = 2 + (int) mPaint.density; // adequate for Material cursors
        return Math.max(0, getPaddingRight() - (cursorWidth - 1));
    }

    public final void draw(Canvas canvas) {
        if (mLayout == null) {
            return;
        }

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

        int top = getTop();
        int bottom = getBottom();

        int extendedPaddingTop = top + paddingTop;

        int entireLayoutHeight = mLayout.getHeight();
        int layoutHeight = entireLayoutHeight;

        if (mMaxLines != 1 || (bottom - top) != (extendedPaddingTop + layoutHeight) || (mGravity & Gravity.VERTICAL_GRAVITY_MASK) != Gravity.TOP) {
            layoutHeight = getLayoutHeight();
        }

        int boxHeight = bottom - paddingBottom - extendedPaddingTop;
        int verticalSpace = boxHeight - layoutHeight;

        if (verticalSpace > 0) {
            switch (mGravity & Gravity.VERTICAL_GRAVITY_MASK) {
                case Gravity.CENTER_VERTICAL:
                    paddingTop += verticalSpace / 2;
                    break;
                case Gravity.BOTTOM:
                    paddingTop += verticalSpace;
                    break;
            }
        }

        int clipTop = 0;
        int clipBottom = bottom;
        if (boxHeight != entireLayoutHeight) {
            clipBottom -= paddingBottom;
            if (mLayout.getLineCount() > mMaxLines) {
                switch (mGravity & Gravity.VERTICAL_GRAVITY_MASK) {
                    case Gravity.TOP: {
                        int vspace = boxHeight - mLayout.getLineTop(mMaxLines);
                        if (vspace > 0) {
                            clipBottom -= vspace;
                        }
                        break;
                    }
                    case Gravity.CENTER_VERTICAL: {
                        int vspace = boxHeight - mLayout.getLineTop(mMaxLines);
                        if (vspace > 0) {
                            clipBottom -= vspace / 2;
                        }
                        break;
                    }
                }
            }
        }

        int clipLeft = getLeft() + paddingLeft;
        int clipRight = getRight() - getFudgedPaddingRight();

        canvas.save();
        canvas.clipRect(clipLeft, clipTop, clipRight, clipBottom);

        if (paddingLeft != 0 || paddingTop != 0) {
            canvas.translate(paddingLeft, paddingTop);
            mLayout.draw(canvas);
        } else {
            mLayout.draw(canvas);
        }

        canvas.restore();
    }

    private static int computeLayoutWidth(Layout layout) {
        float maxWidth = 0;
        for (int i = 0, lineCount = layout.getLineCount(); i != lineCount; ++i) {
            maxWidth = Math.max(maxWidth, layout.getLineMax(i));
        }

        return (int) Math.ceil(maxWidth);
    }

    private int getTextAlignment() {
        return View.TEXT_ALIGNMENT_GRAVITY;
    }

    private Layout.Alignment getLayoutAlignment() {
        Layout.Alignment alignment;
        switch (getTextAlignment()) {
            case View.TEXT_ALIGNMENT_GRAVITY:
                switch (mGravity & Gravity.RELATIVE_HORIZONTAL_GRAVITY_MASK) {
                    case Gravity.START:
                        alignment = Layout.Alignment.ALIGN_NORMAL;
                        break;
                    case Gravity.END:
                        alignment = Layout.Alignment.ALIGN_OPPOSITE;
                        break;
                    case Gravity.LEFT:
                        alignment = ALIGN_LEFT;
                        break;
                    case Gravity.RIGHT:
                        alignment = ALIGN_RIGHT;
                        break;
                    case Gravity.CENTER_HORIZONTAL:
                        alignment = Layout.Alignment.ALIGN_CENTER;
                        break;
                    default:
                        alignment = Layout.Alignment.ALIGN_NORMAL;
                        break;
                }
                break;
/*
            case TEXT_ALIGNMENT_TEXT_START:
                alignment = Layout.Alignment.ALIGN_NORMAL;
                break;
            case TEXT_ALIGNMENT_TEXT_END:
                alignment = Layout.Alignment.ALIGN_OPPOSITE;
                break;
            case TEXT_ALIGNMENT_CENTER:
                alignment = Layout.Alignment.ALIGN_CENTER;
                break;
            case TEXT_ALIGNMENT_VIEW_START:
                alignment = (getLayoutDirection() == LAYOUT_DIRECTION_RTL) ?
                        Layout.Alignment.ALIGN_RIGHT : Layout.Alignment.ALIGN_LEFT;
                break;
            case TEXT_ALIGNMENT_VIEW_END:
                alignment = (getLayoutDirection() == LAYOUT_DIRECTION_RTL) ?
                        Layout.Alignment.ALIGN_LEFT : Layout.Alignment.ALIGN_RIGHT;
                break;
            case TEXT_ALIGNMENT_INHERIT:
                // This should never happen as we have already resolved the text alignment
                // but better safe than sorry so we just fall through
                */
            default:
                alignment = Layout.Alignment.ALIGN_NORMAL;
                break;
        }

        return alignment;
    }

    private Layout ensureStaticLayout(int desiredWidth) {
        Layout layout = mLayout;
        TextUtils.TruncateAt ellipsize = mEllipsize;
        Layout.Alignment alignment = getLayoutAlignment();

        if (layout instanceof StaticLayout && mLayoutIncludeFontPadding == mIncludeFontPadding &&
                mLayoutMaxLines == mMaxLines && mLayoutEllipsize == ellipsize &&
                mLayoutAlignment == alignment) {
            // TODO: reuse layout when
            // a) mLayoutWidth <= desiredWidth <= mlayout.getWidth() (but not always?)
            // b) paragraph width <= desiredWidth
            if (desiredWidth == mLayout.getWidth()) {
                return mLayout;
            }
        }

        // This is not always using mMaxLines, but it should - for efficiency.
        StaticLayout.Builder builder = StaticLayout.Builder.obtain(mText, 0, mText.length(), mPaint, Math.max(desiredWidth, 0))
                .setAlignment(alignment)
                .setIncludePad(mIncludeFontPadding)
                .setBreakStrategy(Layout.BREAK_STRATEGY_HIGH_QUALITY)
                .setHyphenationFrequency(Layout.HYPHENATION_FREQUENCY_NORMAL);
        if (ellipsize != null) {
            builder.setEllipsize(ellipsize)
                    .setEllipsizedWidth(desiredWidth)
                    .setMaxLines(mMaxLines);
        }

        layout = builder.build();

        mLayout = layout;
        mLayoutWidth = computeLayoutWidth(layout);
        mLayoutIncludeFontPadding = mIncludeFontPadding;
        mLayoutEllipsize = mEllipsize;
        mLayoutAlignment = alignment;
        mLayoutMaxLines = mMaxLines;

        return layout;
    }

    private void ensureBoringLayout(int desiredWidth) {
        Layout.Alignment alignment = getLayoutAlignment();
        int textWidth = Math.max(desiredWidth, 0);
        BoringLayout layout = BoringLayout.make(
                mText,
                mPaint,
                textWidth,
                alignment,
                1.0f,
                0.0f,
                mBoringMetrics,
                mIncludeFontPadding/*,
                mEllipsize,
                textWidth*/);

        if (layout.getLineCount() != 1) {
            throw new RuntimeException("wtf");
        }

        if (computeLayoutWidth(layout) != mBoringMetrics.width) {
            throw new RuntimeException("wtf");
        }

        mLayout = layout;
        mLayoutWidth = mBoringMetrics.width;
    }

    public final void setText(CharSequence text) {
        if (text == null) {
            text = "";
        }

        if (mText == text) {
            return;
        }

        mText = text;
        mBoringMetrics = UNKNOWN_BORING;
        mLayout = null;
        requestLayout();
    }

    public final void setMaxLines(int maxLines) {
        if (mMaxLines == maxLines) {
            return;
        }

        mMaxLines = maxLines;
        requestLayout();
    }

    private int getParagraphWidth() {
        return (int) Math.ceil(Layout.getDesiredWidth(mText, mPaint));
    }

    public final void measure(int widthMeasureSpec, int heightMeasureSpec) {
        BoringLayout.Metrics metrics = mBoringMetrics;
        if (metrics == UNKNOWN_BORING) {
            metrics = BoringLayout.isBoring(mText, mPaint, BORING_METRICS);
            if (metrics != null) {
                BORING_METRICS = null;
            }
            mBoringMetrics = metrics;
        }

        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);

        int horizontalPadding = getHorizontalPadding();

        final int desiredTextWidth;
        if (metrics == null) {
            final boolean exactWidth;
            if (widthMode == View.MeasureSpec.UNSPECIFIED) {
                if (BUG_COMPATIBLE) {
                    desiredTextWidth = getParagraphWidth();
                    exactWidth = true;
                } else {
                    desiredTextWidth = Integer.MAX_VALUE;
                    exactWidth = false;
                }
            } else {
                int requestedTextWidth = View.MeasureSpec.getSize(widthMeasureSpec) - horizontalPadding;
                if (STRICT_MEASURE) {
                    // Slightly less performant because it calls Layout.getDesiredWidth but better match TextView behavior
                    exactWidth = true;
                    if (widthMode == View.MeasureSpec.EXACTLY) {
                        desiredTextWidth = requestedTextWidth;
                    } else { // AT_MOST
                        desiredTextWidth = Math.min(requestedTextWidth, getParagraphWidth());
                    }
                } else {
                    // Slightly better performant
                    exactWidth = widthMode == View.MeasureSpec.EXACTLY;
                    desiredTextWidth = requestedTextWidth;
                }

                if (exactWidth) {
                    if (heightMode == View.MeasureSpec.EXACTLY) {
                        setMeasuredDimensions(desiredTextWidth + horizontalPadding, View.MeasureSpec.getSize(heightMeasureSpec));
                        return;
                    }
                }
            }

            measureWithStaticLayout(desiredTextWidth, heightMeasureSpec, exactWidth);
            return;
        }

        final boolean cutOff;

        if (widthMode == View.MeasureSpec.UNSPECIFIED) {
            desiredTextWidth = metrics.width;
            cutOff = false;
        } else {
            int textWidth = metrics.width;
            int requestedWidth = View.MeasureSpec.getSize(widthMeasureSpec);
            int requestedTextWidth = requestedWidth - horizontalPadding;
            if (textWidth <= requestedTextWidth) {
                cutOff = false;
                if (widthMode == View.MeasureSpec.EXACTLY) {
                    desiredTextWidth = requestedTextWidth;
                } else { // AT_MOST
                    desiredTextWidth = textWidth;
                }
            } else if (mMaxLines == 1) {
                // optional optimization that avoids creating StaticLayout
                cutOff = mText.length() > 1 && mEllipsize == null; // cannot cut off 1 character
                desiredTextWidth = requestedTextWidth;
            } else {
                if (heightMode == View.MeasureSpec.EXACTLY) {
                    setMeasuredDimensions(requestedWidth, View.MeasureSpec.getSize(heightMeasureSpec));
                } else { // AT_MOST
                    measureWithStaticLayout(requestedTextWidth, heightMeasureSpec, true);//widthMode == View.MeasureSpec.EXACTLY??
                }
                return;
            }
        }

        final int desiredHeight;
        if (heightMode == View.MeasureSpec.EXACTLY) {
            desiredHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        } else {
            int height = getBoringLayoutMetricsHeight(metrics, cutOff) + getVerticalPadding();
            if (heightMode == View.MeasureSpec.UNSPECIFIED) {
                desiredHeight = height;
            } else { // AT_MOST
                desiredHeight = Math.min(View.MeasureSpec.getSize(heightMeasureSpec), height);
            }
        }

        setMeasuredDimensions(desiredTextWidth + horizontalPadding, desiredHeight);
    }

    private void measureWithStaticLayout(int desiredTextWidth, int heightMeasureSpec, boolean exactWidth) {
        // Text is capable of being Boring, but we didn't fit into requested width so creating a StaticLayout instead
        Layout layout = ensureStaticLayout(desiredTextWidth);

        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);

        final int desiredHeight;
        if (heightMode == View.MeasureSpec.EXACTLY) {
            desiredHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        } else {
            int actualHeight = getLayoutHeight() + getVerticalPadding();
            if (heightMode == View.MeasureSpec.UNSPECIFIED) {
                desiredHeight = actualHeight;
            } else { // AT_MOST
                desiredHeight = Math.min(View.MeasureSpec.getSize(heightMeasureSpec), actualHeight);
            }
        }

        if (!exactWidth) {
            desiredTextWidth = Math.min(desiredTextWidth, mLayoutWidth);
        }

        setMeasuredDimensions(desiredTextWidth + getHorizontalPadding(), desiredHeight);
    }

    private int getLayoutHeight() {
        int numLines = mLayout.getLineCount();
        return mLayout.getLineBottom(Math.min(numLines, mMaxLines) - 1);
    }

    private int getBoringHeight() {
        BoringLayout.Metrics boringMetrics = mBoringMetrics;
        if (boringMetrics == null) {
            boringMetrics = BoringLayout.isBoring(mText, mPaint, BORING_METRICS);
            if (boringMetrics == null) {
                return 0;
            }

            BORING_METRICS = null;
            mBoringMetrics = boringMetrics;
        }

        return getBoringLayoutMetricsHeight(boringMetrics, true /* doesn't really matter */);
    }

    private int getBoringLayoutMetricsHeight(BoringLayout.Metrics metrics, boolean cutOff) {
        if (mMaxLines == 0) {
            return 0;
        }
        if (mIncludeFontPadding) {
            if (cutOff) {
                // simulates height of 1-line StaticLayout that supposed to have > 1 lines.
                return metrics.descent - metrics.top;
            } else {
                return metrics.bottom - metrics.top;
            }
        } else {
            return metrics.descent - metrics.ascent;
        }
    }

    public final void setIncludeFontPadding(boolean includeFontPadding) {
        if (mIncludeFontPadding == includeFontPadding) {
            return;
        }
        mIncludeFontPadding = includeFontPadding;
        requestLayout();
        invalidate();
    }

    public final void setEllipsize(TextUtils.TruncateAt ellipsize) {
        if (mEllipsize == ellipsize) {
            return;
        }
        mEllipsize = ellipsize;
        requestLayout();
        invalidate();
    }

    public final void setGravity(int gravity) {
        if ((gravity & Gravity.RELATIVE_HORIZONTAL_GRAVITY_MASK) == 0) {
            gravity |= Gravity.START;
        }
        if ((gravity & Gravity.VERTICAL_GRAVITY_MASK) == 0) {
            gravity |= Gravity.TOP;
        }
        if (mGravity == gravity) {
            return;
        }
        mGravity = gravity;
        // TODO: don't always request a new layout because gravity doesn't affect layout metrics
        requestLayout();
        invalidate();
    }
}
